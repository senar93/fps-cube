﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RaycastShooter {

	public bool enableFire = true;
	public int rayCastLenght = 500;
	public float fireRate = 1f;
	[SerializeField] private float lastFireTime = 0f;
	public Transform raySpawnPointer;
	public Transform rayDirection;
	//informazioni sul raggio di dispersione

	public string[] layerMask;
	public int layerMaskNumber;

	private RaycastHit hitTarget;
	private bool hitSomething = false;

	/// <summary>
	/// verifica se è possibile sparare: se lo sparo è abilitato e il tempo tra un colpo e l'altro è trascorso
	/// </summary>
	/// <returns></returns>
	public bool CanFireCheck() {
		return enableFire && (lastFireTime + fireRate) <= Time.time;
	}

	/// <summary>
	/// verifica se è stato colpito qualcosa
	/// </summary>
	/// <returns></returns>
	public bool HitSomething() {
		return hitSomething;
	}

	/// <summary>
	/// restituisce l'ultimo target colpito dal raycast
	/// </summary>
	/// <returns>oggetto colpito dal raycast</returns>
	public RaycastHit GetHitTarget() {
		return hitTarget;
	}

	/// <summary>
	/// lancia il raycast
	/// </summary>
	/// <param name="updateLayerMaskNumber">TRUE: calcola layerMaskNumber al lancio della funzione</param>
	public void Fire(bool updateLayerMaskNumber = true) {
		if (CanFireCheck()) {
			lastFireTime = Time.time;
			if (updateLayerMaskNumber)
				layerMaskNumber = LayerMask.GetMask(layerMask);
			if (Physics.Raycast(raySpawnPointer.position, rayDirection.forward, out hitTarget, rayCastLenght, layerMaskNumber)) {
				hitSomething = true;
			}
		}
	}

	/// <summary>
	/// reimposta RaycastShooter perdendo le informazioni del ultimo Fire()
	/// </summary>
	public void Reset() {
		hitSomething = false;
	}

}
