﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {

	public AbstractInputSource inputSource;
	//public AbstractInputSourceCrosshair inputSourceCrosshair;
	public MouseLook mouse;

	public Speed speedPrimaryAxis;
	public Speed speedSecondaryAxis;
	public float jumpForce;

	public bool enabledMovement = true;
	public bool enabledJump = true;
	public bool enabledCameraRotation = true;

	[SerializeField] private float checkOnAirRaycastLenght = 2;
	/// <summary>
	/// indica il tmepo minimo che deve intercorrere tra quando il player si trova in aria e quando potrebbe potenzialmente saltare di nuovo
	/// </summary>
	[SerializeField] private float jumpCooldown = 0.25f;
	[SerializeField] private float lastOnAirTime = 0;
	public string[] jumpLayerMask;
	public int jumpLayerMaskNumber;

	public bool onAirFlag = false;

	private Rigidbody rb;
	[SerializeField] private GetNormal getNormalScript;
	[SerializeField] private Transform playerTransform;
	[SerializeField] private Transform cameraTransform;



	private void Movement() {
		speedPrimaryAxis.Update(inputSource.GetAxisPrimary());
		speedSecondaryAxis.Update(inputSource.GetAxisSecondary());
		Vector3 velocity = (playerTransform.right * speedSecondaryAxis.speedAct) + (playerTransform.forward * speedPrimaryAxis.speedAct);
		rb.MovePosition(rb.position + velocity*Time.deltaTime);
	}


	public void Jump(float jF){
		if (inputSource.GetAxisJump() > 0 && !onAirFlag && (lastOnAirTime + jumpCooldown) <= Time.time) {
			rb.AddForce(getNormalScript.result * jF, ForceMode.Impulse);
			onAirFlag = true;
			lastOnAirTime = Time.time;
		}
	}

	private void CheckOnAir() {
		RaycastHit hit;
		if (Physics.Raycast(playerTransform.position, -getNormalScript.result, out hit, checkOnAirRaycastLenght)) {
			onAirFlag = false;
		} else {
			onAirFlag = true;
		}
	}


	private void CameraRotation() {
		mouse.UpdateCursorLock();
		mouse.LookRotation(playerTransform, cameraTransform);
	}


	// Use this for initialization
	void Start () {
		inputSource = new Keyboard_InputSource();
		jumpLayerMaskNumber = LayerMask.GetMask(jumpLayerMask);
		rb = playerTransform.gameObject.GetComponent<Rigidbody>();
		mouse.Init(playerTransform, cameraTransform);
	}
	
	// Update is called once per frame
	void Update () {
		CheckOnAir();
		if (enabledCameraRotation)
			CameraRotation();
		if(enabledMovement)
			Movement();
		if(enabledJump)
			Jump(jumpForce);
	}


}
