﻿/*
 * per funzionare il proiettile deve colpire un Collider settato come Trigger, altrimenti la collisione viene ignorata
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletDamageScript : MonoBehaviour {

	public GameObject owner;
	public Damage damage;
	


	/// <summary>
	/// infligge danni al target in base ai parametri contenuti in damage
	/// </summary>
	/// <param name="target">target a cui infliggere danni</param>
	public void DealDamage(GameObject target) {
		PartHealthScript tmpPartHealth = target.GetComponent<PartHealthScript>();
		if(tmpPartHealth != null) {
			tmpPartHealth.DealDamage(damage);
		}
	}


	//DA IMPLEMENTARE
	/* deve:
	 * - capire se è abilitato il fuoco amico, e comportarsi di conseguenza
	 * - controllare se il proiettile può o meno danneggiare chi lo ha sparato
	 */
	//attualmente controlla solo se il giocatore che è stato colpito è un giocatore e non è lo stesso che ha sparato
	public bool CheckIfCanDamageTarget(GameObject target) {
		PartHealthScript tmpPartHealth = target.GetComponent<PartHealthScript>();
		if (tmpPartHealth != null && tmpPartHealth.player != owner.gameObject)
			return true;

		return false;
	}

	//DA IMPLEMENTARE
	/* varie condizioni:
	 * - quali oggetti bloccano il proiettile
	 * - se può trapassarne alcuni
	 * - se farà quindi danno una o più volte, se si ogni quanto
	 * - ecc
	 */
	/// <summary>
	/// controlla se il proiettile deve essere distrutto o meno
	/// </summary>
	/// <param name="target">oggetto colpito</param>
	/// <returns></returns>
	public bool CheckIfBulletBeDestroy(GameObject target) {
		PartHealthScript tmpPartHealth = target.GetComponent<PartHealthScript>();
		if (tmpPartHealth != null && tmpPartHealth.player == owner.gameObject) {
			return false;
		}
		return true;

	}



	public void OnTriggerEnter(Collider target) {
		if(CheckIfCanDamageTarget(target.gameObject)) {
			DealDamage(target.gameObject);
		}
		if(CheckIfBulletBeDestroy(target.gameObject)) {
			Debug.Log("Hit " + target.gameObject);
			Destroy(this.gameObject);
		}
	}



}
