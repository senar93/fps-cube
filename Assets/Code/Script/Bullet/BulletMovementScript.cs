﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovementScript : MonoBehaviour {

	public Vector3 target;
	public Speed speed;

	private float startTime = 0;
	private float distance;
	private Vector3 startingPoint;


	void Start() {
		startTime = Time.time;
		distance = Vector3.Distance(transform.position, target);
		startingPoint = this.transform.position;
	}

	// Update is called once per frame
	void Update () {
		speed.Update(1);
		float distanceCovered = (Time.time - startTime) * speed.speedAct;
		transform.position = Vector3.Lerp(startingPoint, target, distanceCovered / distance);
	}
}
