﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartHealthScript : MonoBehaviour {
	
	/// <summary>
	/// puntatore al player che possiede la parte
	/// </summary>
	public PlayerHealthScript player;

	/// <summary>
	/// hp della parte
	/// </summary>
	public Health health;
	/// <summary>
	/// indica se la parte è stata distrutta o meno
	/// </summary>
	public bool isDestroyed = false;

	/// <summary>
	/// indica il fattore con cui i danni inflitti alla parte vengono trasferiti al giocatore
	/// </summary>
	public float transferedDamageScale = 1f;
	/// <summary>
	/// danni addizionali inflitti quando la parte viene distrutta
	/// </summary>
	public float transferedDamageOnDestroyed = 0f;

	public bool deleteObjectIfDestroyed = true;


	/// <summary>
	/// sottra hp alla parte in base agli attributi dell oggetto di tipo Damage passato come parametro
	/// </summary>
	/// <param name="value">danni da infliggere alla parte</param>
	public void DealDamage(Damage value) {
		health.hpAct -= value.damage;

		if (!isDestroyed && health.IsDead()) {
			isDestroyed = true;
			player.DealDamage( (health.hpAct + value.damage) * transferedDamageScale * value.damageTransferedScale
							   + transferedDamageOnDestroyed + value.extraDamageOnDeath);
			if (deleteObjectIfDestroyed)
				DeleteThis();
		} else {
			player.DealDamage(value.damage * value.damageTransferedScale * transferedDamageScale);
		}

	}


	public void DeleteThis() {
		Destroy(this.gameObject);
	}



}
