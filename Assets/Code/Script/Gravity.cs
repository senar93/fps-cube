﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// gravità soggettiva del oggetto
/// lo script deve SEMPRE essere eseguito dopo "CameraGlobalDirectionScript" e PRIMA di "DefaultTime"
/// </summary>
public class Gravity : MonoBehaviour {

	public float gravity = 5;
	public float rotSpeed = 0.03f;
	public Vector3 dir = new Vector3(0, 0, 0);

	public GameObject target;
	[SerializeField] private GetNormal getNormal;
    private Rigidbody rb;

	// Use this for initialization
	void Start() {
		rb = target.GetComponent<Rigidbody>();

    }


	// Update is called once per frame
	void Update() {
        dir = Vector3.Lerp(dir,getNormal.result,(Time.time - getNormal.gotTime) * rotSpeed);
		rb.AddForce(-dir * gravity);
        target.transform.up = Vector3.Lerp(target.transform.up, dir, (Time.time - getNormal.gotTime) * rotSpeed);
        // AGGIUNGERE LA ROTATION DELLA CAMERA

    }

}