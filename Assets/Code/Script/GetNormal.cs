﻿/*
	public Vector3 result;
	public int layer_mask;
	public float gotTime = 0f;
	public int rayCastLenght = 500;

    public Transform raySpawnPointer;


	// Use this for initialization
	void Start() {
		layer_mask = LayerMask.GetMask("OnlyRaycast");
		result = new Vector3 (0, 1, 0);
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKeyDown(KeyCode.T)) {
			RaycastHit hit;
			if (Physics.Raycast(transform.position, raySpawnPointer.forward, out hit, rayCastLenght, layer_mask)) {
				result = hit.normal;
				gotTime = Time.time;
			}
		}
	}
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class GetNormal : MonoBehaviour {

	public Vector3 result;
	public RaycastShooter rayShooter;
	public float gotTime;
	private AbstractInputSource inputSource;
    [SerializeField] private PlayerMovementScript movement;
    // Use this for initialization
    void Start() {
		result = new Vector3 (0, 1, 0);
		rayShooter.layerMaskNumber = LayerMask.GetMask(rayShooter.layerMask);
		inputSource = new Keyboard_InputSource();
	}

	// Update is called once per frame
	void Update() {
		if (inputSource.GetAxisGravityGun() > 0) {
			rayShooter.Reset();
			rayShooter.Fire(false);
			if (rayShooter.HitSomething()) {
				result = rayShooter.GetHitTarget().normal;
				gotTime = Time.time;
			}
		}
	}

}
