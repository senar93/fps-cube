﻿/*
 * prenda la rotazione di camera e player in modo da avere gli assi coerenti, da usare per i raycast che dovranno seguire i proiettili (es. la gravity gun)
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// permette di prendere gli assi coerenti con la rotazione complessiva del oggetto e della camera
/// lo script deve essere SEMPRE eseguito prima di "Gravity"
/// </summary>
public class CameraGlobalDirectionScript : MonoBehaviour {

	/// <summary>
	/// puntatore alla camera del player
	/// </summary>
	[SerializeField] private Transform playerCamera;

	public Vector3 offsetPosition;
	public Vector3 offsetRotation;

	private Quaternion tmpRotation = Quaternion.identity;
	

	private void Start() {
		offsetPosition = this.transform.position;
	}


	void LateUpdate () {
		this.transform.position = new Vector3(playerCamera.position.x + offsetPosition.x,
											  playerCamera.position.y + offsetPosition.y,
											  playerCamera.position.z + offsetPosition.z);

		tmpRotation.eulerAngles = playerCamera.rotation.eulerAngles + offsetRotation;
		this.transform.rotation = tmpRotation;

	}


}
