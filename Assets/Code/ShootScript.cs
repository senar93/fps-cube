﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootScript : MonoBehaviour {

	public AbstractInputSource inputSource;
	public GameObject bulletPrefab;
	public RaycastShooter raycastShooter;
	public int bulletNumberAct;
	public int bulletNumberMax = 50;




	public bool CanShoot() {
		if (raycastShooter.CanFireCheck() && bulletNumberAct > 0) {
			return true;
		}
		return false;
	}


	public void Shoot() {
		if(CanShoot()) {
			raycastShooter.Reset();
			raycastShooter.Fire(false);
			if(raycastShooter.HitSomething()) {
				GameObject tmpBullet = Instantiate(bulletPrefab);
				tmpBullet.GetComponent<BulletMovementScript>().target = raycastShooter.GetHitTarget().point;
				tmpBullet.GetComponent<BulletDamageScript>().owner = this.gameObject;
				tmpBullet.transform.position = raycastShooter.raySpawnPointer.position;
				tmpBullet.transform.rotation = raycastShooter.rayDirection.rotation;
				//Debug.Log("bullet position : " + tmpBullet.transform.position);
				//Debug.Log("camera position : " + raycastShooter.raySpawnPointer.position);
			}
		}
	}


	// Use this for initialization
	void Start () {
		inputSource = new Keyboard_InputSource();
		raycastShooter.layerMaskNumber = LayerMask.GetMask(raycastShooter.layerMask);
		bulletNumberAct = bulletNumberMax;
	}
	
	// Update is called once per frame
	void Update () {
		if(inputSource.GetAxisShoot() > 0) {
			Shoot();
		}
	}

}
