﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyboard_InputSource : AbstractInputSource {

	public override float GetAxisPrimary() {
		return Input.GetAxis("Vertical");
	}

	public override float GetAxisSecondary() {
		return Input.GetAxis("Horizontal");
	}

	public override float GetAxisJump() {
		return Input.GetAxis("Jump");
	}

	public override float GetAxisShoot() {
		return Input.GetAxis("Fire1");
	}

	public override float GetAxisGravityGun() {
		return Input.GetAxis("GravityGun");
	}

}
