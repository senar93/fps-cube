﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractInputSource {

	public abstract float GetAxisPrimary();
	public abstract float GetAxisSecondary();
	public abstract float GetAxisJump();

	public abstract float GetAxisShoot();
	public abstract float GetAxisGravityGun();


}
