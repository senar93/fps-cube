﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// classe che implementa la generica statistica health
/// </summary>
[System.Serializable] public class Health {
	/// <summary>
	/// soglia che, se raggiunta da hpAct, provoca il ritorno di true da IsDead()
	/// </summary>
	public static float DEATH_THRESHOLD = 0;

	/// <summary>
	/// valore attuale degli hp, solo per uso interno, modificato da hpAct
	/// </summary>
	[SerializeField] private float _hpAct;
	/// <summary>
	/// valore massimo degli hp
	/// </summary>
	[SerializeField] private float hpMax;

	/// <summary>
	/// valore attuale degli hp
	/// </summary>
	public float hpAct {
		get {
			return _hpAct;
		}
		set {
			if (value < hpMax)
				_hpAct = value;
			else
				_hpAct = hpMax;
		}
	}




	/// <summary>
	/// restituisce gli hp massimi
	/// </summary>
	/// <returns>hp massimi</returns>
	public float GetHpMax() {
		return hpMax;
	}


	/// <summary>
	/// imposta gli hp massimi del oggetto ad un nuovo valore
	/// </summary>
	/// <param name="value">nuovo valore degli hp massimi</param>
	/// <param name="mode">modalità in cui la variazione di hp massimi influenza gli hp attuali</param>
	public void SetHpMax(float value, EnumSetStatMaxMode mode = EnumSetStatMaxMode.Standard) {
		switch(mode) {
			case EnumSetStatMaxMode.Standard: {
				hpMax = value;
				if (hpAct > hpMax)
					hpAct = hpMax;
				break;
			}

			case EnumSetStatMaxMode.Additive: {
				float previousHpMax = hpMax;
				hpMax = value;
				hpAct += hpMax - previousHpMax;
				break;
			}

			case EnumSetStatMaxMode.Rate: {
				float previousHpMax = hpMax;
				hpMax = value;
				hpMax *= hpMax / previousHpMax;
				break;
			}

		}

	}


	/// <summary>
	/// restitusice se l'oggetto è vivo o morto
	/// </summary>
	/// <returns>TRUE:		l'oggetto è vivo, FALSO altrimenti</returns>
	public bool IsDead() {
		if (hpAct <= DEATH_THRESHOLD)
			return true;
		else
			return false;
	}



}
