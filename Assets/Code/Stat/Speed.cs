﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// classe che implementa la velocità su un asse di un oggetto
/// </summary>
[System.Serializable]
public class Speed {

	/// <summary>
	/// velocità attuale
	/// </summary>
	public float speedAct;
	/// <summary>
	/// velocità massima con input positivo
	/// </summary>
	[SerializeField] private float speedMaxUp;
	/// <summary>
	/// velocità massima con input negativo o nullo
	/// </summary>
	[SerializeField] private float speedMaxDown;
	/// <summary>
	/// accellerazione con input > 0
	/// </summary>
	public float accelerationUp;
	/// <summary>
	/// accelerazione (decellerazione) con input negativo o nullo
	/// </summary>
	public float accelerationDown;
	/// <summary>
	/// 
	/// </summary>
	public float decelleration;

	public float noInputThreshold = 0.1f;




	/// <summary>
	/// restituisce la velocità massima (input +)
	/// </summary>
	/// <returns>velocità massima</returns>
	public float GetSpeedMaxUp() {
		return speedMaxUp;
	}

	/// <summary>
	/// restituisce la velocità massima (input -, 0)
	/// </summary>
	/// <returns>velocità massima</returns>
	public float GetSpeedMaxDown() {
		return speedMaxDown;
	}

	/// <summary>
	/// imposta la velocità massima del oggetto su un certo asse (input +)
	/// l'accelerazione è sempre impostata in percentuale qualsiasi sia la modalità scelta
	/// </summary>
	/// <param name="value">nuovo valore della velocità massima</param>
	/// <param name="mode">modalità in cui la variazione di velocità massima influenza la velocità attuale</param>
	/// <param name="setAccelerationRate">TRUE: imposta l'accelerazione in percentuale alla variazione di velocità massima</param>
	public void SetSpeedMaxUp(float value, EnumSetStatMaxMode mode = EnumSetStatMaxMode.Standard, bool setAccelerationRate = false) {
		SetSpeedMaxGeneric(ref speedMaxUp, value, mode, setAccelerationRate);
	}

	/// <summary>
	/// imposta la velocità massima del oggetto su un certo asse (input -, 0)
	/// l'accelerazione è sempre impostata in percentuale qualsiasi sia la modalità scelta
	/// </summary>
	/// <param name="value">nuovo valore della velocità massima</param>
	/// <param name="mode">modalità in cui la variazione di velocità massima influenza la velocità attuale</param>
	/// <param name="setAccelerationRate">TRUE: imposta l'accelerazione in percentuale alla variazione di velocità massima</param>
	public void SetSpeedMaxDown(float value, EnumSetStatMaxMode mode = EnumSetStatMaxMode.Standard, bool setAccelerationRate = false) {
		SetSpeedMaxGeneric(ref speedMaxDown, value, mode, setAccelerationRate);
	}



	/// <summary>
	/// DA NON UTILIZARE al di fuori di SetSpeedMaxDown(...) e SetSpeedMaxUp(...)
	/// </summary>
	private void SetSpeedMaxGeneric(ref float varSpeedMax, float value, EnumSetStatMaxMode mode, bool setAccelerationRate) {
		float previousSpeedMax = varSpeedMax;

		switch (mode) {
			case EnumSetStatMaxMode.Standard: {
					varSpeedMax = value;
					if (speedAct > varSpeedMax)
						speedAct = varSpeedMax;
					if (setAccelerationRate)
						accelerationDown *= varSpeedMax / previousSpeedMax;
					break;
				}

			case EnumSetStatMaxMode.Additive: {
					varSpeedMax = value;
					speedAct += varSpeedMax - previousSpeedMax;
					if (setAccelerationRate)
						accelerationDown *= varSpeedMax / previousSpeedMax;
					break;
				}

			case EnumSetStatMaxMode.Rate: {
					varSpeedMax = value;
					speedAct *= varSpeedMax / previousSpeedMax;
					if (setAccelerationRate)
						accelerationDown *= varSpeedMax / previousSpeedMax;
					break;
				}
		}
	}



	/// <summary>
	/// aggiorna il valore di velocità attuale in base all input
	/// </summary>
	/// <param name="input">valore assiale del input (-1 -> +1)</param>
	public void Update(float input) {
		if (input < noInputThreshold && input > -noInputThreshold) {
			if (speedAct > 0) {
				speedAct -= decelleration * Time.deltaTime;
				if (speedAct < 0)
					speedAct = 0;
			} else if (speedAct < 0) {
				speedAct += decelleration * Time.deltaTime;
				if (speedAct > 0)
					speedAct = 0;
			}
		} else if (input > 0) {
			speedAct += input * accelerationUp * Time.deltaTime;
			if (speedAct > speedMaxUp)
				speedAct = speedMaxUp;
		} else if (input < 0) {
			speedAct += input * accelerationDown * Time.deltaTime;
			if (speedAct < -speedMaxDown)
				speedAct = -speedMaxDown;
		} 
	}


}
