﻿/// <summary>
/// indica la modalità con la quale vengono modificati gli hp massimi
/// </summary>
public enum EnumSetStatMaxMode {
	/// <summary>
	/// modifica gli hp massimi, se il valore è minore di quelli attuali abbassa quelli attuali
	/// </summary>
	Standard = 0,

	/// <summary>
	/// modifica gli hp massimi e attuali dello stesso valore
	/// </summary>
	Additive = 1,

	/// <summary>
	/// modifica gli hp massimi, e della stessa percentuale di incremento/decremento gli hp attuali
	/// </summary>
	Rate = 2
}